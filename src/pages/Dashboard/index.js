import React, { useState, useEffect, useMemo } from 'react';
import { Link } from 'react-router-dom';
import api from '../../services/api'
import socket from 'socket.io-client'

import './styles.css'

const Dashboard = ({ history }) => {

    const [spots, setSpots] = useState([])
    const [requests, setRequests] = useState([])

    const user_id = localStorage.getItem('user')

    const io = useMemo(() => socket('https://aircnc--backend.herokuapp.com', {
        query: { user_id }
    }), [user_id])

    useEffect(() => {
        io.on('booking_req', data => {
            setRequests([...requests, data])
        })
    }, [requests, io])

    useEffect(() => {
        async function loadSpots() {
            const user_id = localStorage.getItem('user')

            const res = await api.get('/dashboard', { headers: { user_id } })
            setSpots(res.data)
        }

        loadSpots();
    }, [])

    async function handleAccept(id) {
        await api.post('/bookings/' + id + '/approvals')
        setRequests(requests.filter(r => r._id !== id))
    }

    async function handleReject(id) {
        await api.post('/bookings/' + id + '/rejections')
        setRequests(requests.filter(r => r._id !== id))
    }

    function sair() {
        localStorage.removeItem('user')
        history.push('/')
    }

    return (
        <>
            <p>
                Dashboard
            </p>

            <ul className="notifications">
                {requests.map((req, i) => (
                    <li key={i}>
                        <p>
                            <b>{req._user.email}</b> está solicitando uma reserva em <b>{req._spot.company}</b> para a data <b>{req.date}</b>
                        </p>

                        <button className="accept" onClick={() => handleAccept(req._id)}>Aceitar</button>
                        <button className="reject" onClick={() => handleReject(req._id)}>Rejeitar</button>
                    </li>
                ))}
            </ul>

            <ul className="spot-list">
                {spots.map((spot, i) => {
                    return (
                        <li key={i}>
                            <header style={{ backgroundImage: `url(${spot.thumbnail_url})` }} />
                            <strong>{spot.company}</strong>
                            <span>{spot.price ? `R$ ${spot.price}/dia` : `GRATUITO`}</span>
                        </li>
                    )
                })}
            </ul>

            <Link to='/new'>
                <button className="btn">
                    Cadastrar um novo spot
                </button>
            </Link>

            <Link to='/'>
                <button className="btnSair" onClick={sair}>
                    Sair
                </button>
            </Link>
        </>
    )
}

export default Dashboard