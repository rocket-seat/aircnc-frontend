import React, { useState } from 'react';
import api from '../../services/api'

const Login = ({ history }) => {

    const [email, setEmail] = useState('')

    async function handleSubmit(e) {
        e.preventDefault();

        const res = await api.post('/sessions', { email })
        const { _id } = res.data;

        localStorage.setItem("user", _id)
        history.push('/dashboard')
    }

    return (
        <>
            <p>
                Ofereça <b>spots</b> para programadores e encontre <b>talentos</b> para sua empresa
            </p>

            <form onSubmit={handleSubmit}>
                <label htmlFor="email">E-mail</label>
                <input type="email" id="email" value={email} onChange={(e) => setEmail(e.target.value)} placeholder="Informe seu e-mail" />

                <button className="btn" type="submit">Entrar</button>
            </form>
        </>
    )
}

export default Login